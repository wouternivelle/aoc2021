import 'dart:io';

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}

String part1(List<String> lines) {
  List<List<Octopus>> matrix = [];

  lines.forEach((line) {
    List<Octopus> octopuses = [];
    characters(line).forEach((char) {
      octopuses.add(Octopus(char, false));
    });
    matrix.add(octopuses);
  });

  int maxRow = matrix.length;
  int maxCol = matrix[0].length;

  int flashes = 0;
  for (int step = 1; step <= 100; step++) {
    // Increase by one
    for (int row = 0; row < maxRow; row++) {
      for (int col = 0; col < maxCol; col++) {
        matrix[row][col].value++;
      }
    }

    // Start checking flashes
    for (int row = 0; row < maxRow; row++) {
      for (int col = 0; col < maxCol; col++) {
        flash(row, col, matrix);
      }
    }

    // Reset flashes
    for (int row = 0; row < maxRow; row++) {
      for (int col = 0; col < maxCol; col++) {
        if (matrix[row][col].flashed) {
          flashes++;
          matrix[row][col].reset();
        }
      }
    }
  }

  return flashes.toString();
}

String part2(List<String> lines) {
  List<List<Octopus>> matrix = [];

  lines.forEach((line) {
    List<Octopus> octopuses = [];
    characters(line).forEach((char) {
      octopuses.add(Octopus(char, false));
    });
    matrix.add(octopuses);
  });

  int maxRow = matrix.length;
  int maxCol = matrix[0].length;

  int step = 1;
  bool synchronized = false;
  while (!synchronized) {
    // Increase by one
    for (int row = 0; row < maxRow; row++) {
      for (int col = 0; col < maxCol; col++) {
        matrix[row][col].value++;
      }
    }

    // Start checking flashes
    for (int row = 0; row < maxRow; row++) {
      for (int col = 0; col < maxCol; col++) {
        flash(row, col, matrix);
      }
    }

    // Reset flashes
    for (int row = 0; row < maxRow; row++) {
      for (int col = 0; col < maxCol; col++) {
        if (matrix[row][col].flashed) {
          matrix[row][col].reset();
        }
      }
    }

    // Check synchronisation
    int first = matrix[0][0].value;
    bool sync = true;
    for (int row = 0; row < maxRow; row++) {
      for (int col = 0; col < maxCol; col++) {
        sync &= first == matrix[row][col].value;
      }
    }
    if (sync) {
      synchronized = true;
    }
    step++;
  }

  return (step - 1).toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

List<int> characters(String toSplit) {
  return toSplit.split('').map((e) => int.parse(e)).toList();
}

class Octopus {
  int value;
  bool flashed;

  Octopus(this.value, this.flashed);

  reset() {
    value = 0;
    flashed = false;
  }
}

printMatrix(List<List<Octopus>> matrix) {
  matrix.forEach((row) {
    String toPrint = '';
    row.forEach((col) {
      toPrint += col.value.toString();
    });
    print(toPrint);
  });
}

flash(int row, int col, List<List<Octopus>> matrix) {
  int maxRow = matrix.length;
  int maxCol = matrix[0].length;

  if (col < 0 || col >= maxCol || row < 0 || row >= maxRow) {
    // Out of bounds do nothing
  } else {
    Octopus current = matrix[row][col];
    if (current.value > 9 && !current.flashed) {
      // Flash
      current.flashed = true;

      // Increment adjacent
      increment(row - 1, col, matrix); // top
      increment(row - 1, col - 1, matrix); // top-left
      increment(row, col - 1, matrix); // left
      increment(row + 1, col - 1, matrix); // left
      increment(row + 1, col, matrix); // bottom
      increment(row + 1, col + 1, matrix); // bottom-right
      increment(row, col + 1, matrix); // right
      increment(row - 1, col + 1, matrix); // top-right

      // Flash adjacent
      flash(row - 1, col, matrix); // top
      flash(row - 1, col - 1, matrix); // top-left
      flash(row, col - 1, matrix); // left
      flash(row + 1, col - 1, matrix); // left
      flash(row + 1, col, matrix); // bottom
      flash(row + 1, col + 1, matrix); // bottom-right
      flash(row, col + 1, matrix); // right
      flash(row - 1, col + 1, matrix); // top-right
    }
  }
}

increment(int row, int col, List<List<Octopus>> matrix) {
  int maxRow = matrix.length;
  int maxCol = matrix[0].length;

  if (col < 0 || col >= maxCol || row < 0 || row >= maxRow) {
    // Out of bounds do nothing
  } else {
    matrix[row][col].value++;
  }
}
