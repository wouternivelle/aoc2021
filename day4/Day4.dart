import 'dart:io';

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}

String part1(List<String> lines) {
  String sequence = lines[0];

  lines.removeAt(0);
  lines.removeAt(0);

  List<Board> boards = [];
  Board? board;
  lines.forEach((element) {
    if (board == null) {
      board = Board();
    }
    if (element == '') {
      boards.add(board!);
      board = null;
    } else {
      board!.addRow(element);
    }
  });
  boards.add(board!);

  String? result;
  int sum = 0;
  List<String> sequences = sequence.split(',');
  for (String seq in sequences) {
    for (Board b in boards) {
      b.mark(int.parse(seq));
      if (b.check()) {
        result = seq;
        sum = b.sumUnmarked();

        print('Winning board:');
        b.draw();

        break;
      }
    }
    if (result != null) {
      break;
    }
  }

  return (int.parse(result!) * sum).toString();
}

String part2(List<String> lines) {
  String sequence = lines[0];

  lines.removeAt(0);
  lines.removeAt(0);

  List<Board> boards = [];
  Board? board;
  lines.forEach((element) {
    if (board == null) {
      board = Board();
    }
    if (element == '') {
      boards.add(board!);
      board = null;
    } else {
      board!.addRow(element);
    }
  });
  boards.add(board!);

  String? result;
  int sum = 0;
  List<String> sequences = sequence.split(',');
  for (String seq in sequences) {
    for (Board b in boards) {
      if (!b.isDone) {
        b.mark(int.parse(seq));
        if (b.check()) {
          b.done();
          board = b;
        }
      }
    }

    List<Board> remainingBoards = boards.where((element) => !element.isDone).toList();
    if (remainingBoards.length == 0) {
      result = seq;
      sum = board!.sumUnmarked();
      break;
    }
  }

  return (int.parse(result!) * sum).toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

class Board {
  List<List<Point>> list = [];
  bool isDone = false;

  addRow(String row) {
    row = row.trim();
    list.add(
        row.split(new RegExp('\\s+')).map((e) => Point(int.parse(e))).toList());
  }

  mark(int mark) {
    list.forEach((row) {
      row.forEach((column) {
        if (column.value == mark) {
          column.marked = true;
        }
      });
    });
  }

  bool check() {
    // Horizontal check
    for (int row = 0; row < 5; row++) {
      bool result = true;
      for (int col = 0; col < 5; col++) {
        result &= list[row][col].marked;
      }
      if (result) {
        return true;
      }
    }

    // Vertical check
    for (int col = 0; col < 5; col++) {
      bool result = true;
      for (int row = 0; row < 5; row++) {
        result &= list[row][col].marked;
      }
      if (result) {
        return true;
      }
    }

    return false;
  }

  int sumUnmarked() {
    int sum = 0;
    list.forEach((row) {
      row.forEach((column) {
        if (!column.marked) {
          sum += column.value;
        }
      });
    });

    return sum;
  }

  draw() {
    list.forEach((row) {
      String draw = '';
      row.forEach((column) {
        draw += column.value.toString() + (column.marked ? 'x ' : '  ');
      });
      print(draw);
    });
  }

  done() {
    isDone = true;
  }
}

class Point {
  int value;
  bool marked = false;

  Point(this.value);
}
