import 'dart:io';
import 'dart:math';

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}

String part1(List<String> lines) {
  List<int> positions = lines[0].split(',').map((e) => int.parse(e)).toList();

  int median = calculateMedian(positions);

  int sum = 0;
  positions.forEach((element) {
    sum += (element - median).abs();
  });
  return sum.toString();
}

String part2(List<String> lines) {
  List<int> positions = lines[0].split(',').map((e) => int.parse(e)).toList();
  int maxPosition = positions.reduce(max);

  int minSum = 0;
  for (int pos = 1; pos < maxPosition; pos++) {
    int sum = 0;
    positions.forEach((element) {
      int difference = (element - pos).abs();
      for (int i = 0; i < difference; i++) {
        sum += i;
      }
      sum += difference;
    });

    if (minSum == 0) {
      minSum = sum;
    } else {
      minSum = min(minSum, sum);
    }
  }

  return minSum.toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

int calculateMedian(List<int> numbers) {
  numbers.sort((a, b) => a.compareTo(b));

  int median;

  int middle = numbers.length ~/ 2;
  if (numbers.length % 2 == 1) {
    median = numbers[middle];
  } else {
    median = ((numbers[middle - 1] + numbers[middle]) / 2.0).round();
  }

  return median;
}
