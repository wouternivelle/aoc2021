import 'dart:io';
import 'dart:math';

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}

String part1(List<String> lines) {
  List<Coords> coords = [];
  lines.forEach((element) {
    List<String> spaces = element.split(' ');
    Coord from = Coord(
        int.parse(spaces[0].split(',')[0]), int.parse(spaces[0].split(',')[1]));
    Coord to = Coord(
        int.parse(spaces[2].split(',')[0]), int.parse(spaces[2].split(',')[1]));

    if (from.x == to.x || from.y == to.y) {
      coords.add(Coords(from, to));
    }
  });

  int maxX = 0;
  int maxY = 0;
  coords.forEach((element) {
    maxX = max(element.maxX(), maxX);
    maxY = max(element.maxY(), maxY);
  });

  List<List<int>> matrix =
      List.generate(maxX + 1, (index) => List.generate(maxY + 1, (index) => 0));

  coords.forEach((element) {
    element.range().forEach((coord) {
      matrix[coord.x][coord.y]++;
    });
  });

  int count = 0;
  matrix.forEach((row) {
    row.forEach((col) {
      if (col >= 2) {
        count++;
      }
    });
  });

  return count.toString();
}

String part2(List<String> lines) {
  List<Coords> coords = [];
  lines.forEach((element) {
    List<String> spaces = element.split(' ');
    Coord from = Coord(
        int.parse(spaces[0].split(',')[0]), int.parse(spaces[0].split(',')[1]));
    Coord to = Coord(
        int.parse(spaces[2].split(',')[0]), int.parse(spaces[2].split(',')[1]));
    coords.add(Coords(from, to));
  });

  int maxX = 0;
  int maxY = 0;
  coords.forEach((element) {
    maxX = max(element.maxX(), maxX);
    maxY = max(element.maxY(), maxY);
  });

  List<List<int>> matrix =
      List.generate(maxX + 1, (index) => List.generate(maxY + 1, (index) => 0));

  coords.forEach((element) {
    element.diagonalRange().forEach((coord) {
      matrix[coord.x][coord.y]++;
    });
  });

  int count = 0;
  matrix.forEach((row) {
    row.forEach((col) {
      if (col >= 2) {
        count++;
      }
    });
  });

  return count.toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

class Coords {
  Coord from;
  Coord to;

  Coords(this.from, this.to);

  int maxX() {
    return max(from.x, to.x);
  }

  int maxY() {
    return max(from.y, to.y);
  }

  List<Coord> range() {
    List<Coord> result = [];

    if (from.x == to.x) {
      int minY = min(from.y, to.y);
      int maxY = max(from.y, to.y);
      while (minY <= maxY) {
        result.add(Coord(from.x, minY));
        minY++;
      }
    } else {
      int minX = min(from.x, to.x);
      int maxX = max(from.x, to.x);
      while (minX <= maxX) {
        result.add(Coord(minX, from.y));
        minX++;
      }
    }

    return result;
  }

  List<Coord> diagonalRange() {
    List<Coord> result = [];

    if (from.x != to.x && from.y != to.y) {
      if (from.x < to.x && from.y < to.y) {
        Coord start = Coord(from.x, from.y);
        result.add(start);
        while (!start.equals(to)) {
          start = Coord(start.x + 1, start.y + 1);
          result.add(start);
        }
      } else if (from.x < to.x && from.y > to.y) {
        Coord start = Coord(from.x, from.y);
        result.add(start);
        while (!start.equals(to)) {
          start = Coord(start.x + 1, start.y - 1);
          result.add(start);
        }
      } else if (from.x > to.x && from.y < to.y) {
        Coord start = Coord(from.x, from.y);
        result.add(start);
        while (!start.equals(to)) {
          start = Coord(start.x - 1, start.y + 1);
          result.add(start);
        }
      } else if (from.x > to.x && from.y > to.y) {
        Coord start = Coord(from.x, from.y);
        result.add(start);
        while (!start.equals(to)) {
          start = Coord(start.x - 1, start.y - 1);
          result.add(start);
        }
      }
    } else {
      result.addAll(range());
    }

    return result;
  }

  @override
  String toString() {
    return '[' +
        from.x.toString() +
        ',' +
        from.y.toString() +
        ']' +
        '[' +
        to.x.toString() +
        ',' +
        to.y.toString() +
        ']';
  }
}

class Coord {
  int x;
  int y;

  Coord(this.x, this.y);

  @override
  String toString() {
    return x.toString() + ',' + y.toString();
  }

  bool equals(Coord obj) {
    return x == obj.x && y == obj.y;
  }
}
