import 'dart:io';
import 'dart:math';

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('example.txt')));
}

String part1(List<String> lines) {
  List<List<int>> matrix = [];

  lines.forEach((element) {
    matrix.add(characters(element));
  });

  List<int> lowPoints = [];
  int maxRow = matrix.length;
  int maxCol = matrix[0].length;
  for (int row = 0; row < maxRow; row++) {
    for (int col = 0; col < maxCol; col++) {
      if (checkLowPoint(row, col, matrix)) {
        lowPoints.add(matrix[row][col]);
      }
    }
  }

  int sum = 0;
  lowPoints.forEach((element) {
    sum += element + 1;
  });

  return sum.toString();
}

String part2(List<String> lines) {
  List<List<int>> matrix = [];

  lines.forEach((element) {
    matrix.add(characters(element));
  });

  List<Point> lowPoints = [];
  int maxRow = matrix.length;
  int maxCol = matrix[0].length;
  for (int row = 0; row < maxRow; row++) {
    for (int col = 0; col < maxCol; col++) {
      if (checkLowPoint(row, col, matrix)) {
        lowPoints.add(Point(row, col));
      }
    }
  }

  List<int> result = [];
  lowPoints.forEach((element) {
    Set<Point> basin = Set();
    basin.add(element);
    search(element.row - 1, element.col, matrix, basin); // top
    search(element.row, element.col - 1, matrix, basin); // left
    search(element.row + 1, element.col, matrix, basin); // bottom
    search(element.row, element.col + 1, matrix, basin); // right
    result.add(basin.length);
  });

  result.sort((a, b) => b.compareTo(a));

  return (result[0] * result[1] * result[2]).toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

List<int> characters(String toSplit) {
  return toSplit.split('').map((e) => int.parse(e)).toList();
}

bool checkLowPoint(int row, int col, matrix) {
  int maxRow = matrix.length;
  int maxCol = matrix[0].length;

  // top
  if (row - 1 >= 0 && matrix[row - 1][col] <= matrix[row][col]) {
    return false;
  }

  // left
  if (col - 1 >= 0 && matrix[row][col - 1] <= matrix[row][col]) {
    return false;
  }

  // bottom
  if (row + 1 < maxRow && matrix[row + 1][col] <= matrix[row][col]) {
    return false;
  }

  // right
  if (col + 1 < maxCol && matrix[row][col + 1] <= matrix[row][col]) {
    return false;
  }

  return true;
}

void search(int row, int col, List<List<int>> matrix, Set<Point> basin) {
  int maxRow = matrix.length;
  int maxCol = matrix[0].length;

  if (col < 0 || col >= maxCol || row < 0 || row >= maxRow) {
    // Do nothing
  } else {
    int current = matrix[row][col];
    if (current == 9) {
      // Do nothing
    } else {
      basin.add(Point(row, col)); // Valid point

      search(row - 1, col, matrix, basin); // top
      search(row, col - 1, matrix, basin); // left
      search(row + 1, col, matrix, basin); // bottom
      search(row, col + 1, matrix, basin); // right
    }
  }
}

class Point {
  int row;
  int col;

  Point(this.row, this.col);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Point &&
          runtimeType == other.runtimeType &&
          row == other.row &&
          col == other.col;

  @override
  int get hashCode => row.hashCode ^ col.hashCode;
}
