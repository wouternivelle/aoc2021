import 'dart:io';

String part1(List<String> lines) {
  List<int> input = lines.map((e) => int.parse(e)).toList();

  int count = 0;
  int previous = 0;
  for (var value in input) {
    if (previous != 0 && previous < value) {
      count++;
    }
    previous = value;
  }

  return count.toString();
}

String part2(List<String> lines) {
  List<int> input = lines.map((e) => int.parse(e)).toList();
  final Map<int, List<int>> map = new Map();
  int index = 1;
  input.forEach((element) {
    map.keys.forEach((entry) {
      if (map[entry]!.length < 3) {
        map[entry]!.add(element);
      }
    });
    if (!map.containsKey(index)) {
      map[index] = [element];
    }

    index++;
  });

  int count = 0;
  int previous = 0;
  map.keys.forEach((key) {
    int total = 0;
    map[key]!.forEach((element) {
      total += element;
    });
    if (previous != 0 && previous < total) {
      count++;
    }
    previous = total;
  });

  return count.toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}
