import 'dart:io';
import 'dart:math';

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}

String part1(List<String> lines) {
  final List<Fish> school = [];
  lines[0].split(',').forEach((timer) {
    school.add(Fish(int.parse(timer)));
  });

  List<Fish> newSchool = [];
  for (int i = 0; i < 80; i++) {
    school.forEach((fish) {
      Fish? newFish = fish.age();
      if (newFish != null) {
        newSchool.add(newFish);
      }
    });

    school.addAll(newSchool);
    newSchool.clear();
  }

  return school.length.toString();
}

String part2(List<String> lines) {
  Map<int, int> timers = new Map();
  for (int i = 0; i < 9; i++) {
    timers[i] = 0;
  }

  final List<Fish> school = [];
  lines[0].split(',').forEach((timer) {
    school.add(Fish(int.parse(timer)));
  });

  school.forEach((element) {
    timers[element.timer] = timers[element.timer]! + 1;
  });

  int newSchool = 0;
  for (int day = 0; day < 256; day++) {
    for (int i = 0; i < 9; i++) {
      if (i == 0) {
        newSchool = timers[i]!;
      } else {
        timers[i - 1] = timers[i]!;
      }
    }
    timers[6] = timers[6]! + newSchool;
    timers[8] = newSchool;
    newSchool = 0;
  }

  int sum = 0;
  timers.values.forEach((element) {
    sum += element;
  });
  return sum.toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

class Fish {
  static int maxTimer = 6;
  static int newTimer = 8;
  int timer;

  Fish(this.timer);

  Fish? age() {
    timer--;
    if (timer < 0) {
      timer = maxTimer;
      return Fish(newTimer);
    }
    return null;
  }
}
