import 'dart:io';
import 'dart:math';

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}

String part1(List<String> lines) {
  List<Entry> entries = [];
  lines.forEach((element) {
    entries.add(Entry([], element.split(' | ')[1].split(' ').toList()));
  });
  int count = 0;
  entries.forEach((entry) {
    entry.values.forEach((value) {
      if ([2, 4, 3, 7].contains(value.length)) {
        count++;
      }
    });
  });

  return count.toString();
}

String part2(List<String> lines) {
  List<Entry> entries = [];
  lines.forEach((element) {
    entries.add(Entry(element.split(' | ')[0].split(' ').toList(), element.split(' | ')[1].split(' ').toList()));
  });

  int sum = 0;
  entries.forEach((entry) {
    Map<int, String> mapping = Map();
    entry.signals.forEach((element) {
      if (element.length == 2) {
        mapping[1] = element;
      } else if (element.length == 4) {
        mapping[4] = element;
      } else if (element.length == 3) {
        mapping[7] = element;
      } else if (element.length == 7) {
        mapping[8] = element;
      }
    });
    entry.signals.remove(mapping[1]!);
    entry.signals.remove(mapping[4]!);
    entry.signals.remove(mapping[7]!);
    entry.signals.remove(mapping[8]!);

    List<String> seven = characters(mapping[7]!);
    seven.removeWhere((element) => characters(mapping[1]!).contains(element));
    Digit digit = Digit();
    digit.order[0] = minus(mapping[7]!, mapping[1]!)[0];

    // 9
    entry
        .signals
        .where((element) => element.length == 6)
        .forEach((element) {
      List<String> result = minus(mapping[8]!, element);
      result = minusString(result, mapping[4]!);
      if (result.length == 1) {
        mapping[9] = element;
        digit.order[4] = result[0];
      }
    });
    entry.signals.remove(mapping[9]!);

    // 2
    entry
        .signals
        .where((element) => element.length == 5)
        .forEach((element) {
      List<String> result = minus(mapping[4]!, element);
      if (result.length == 2) {
        mapping[2] = element;
      }
    });
    entry.signals.remove(mapping[2]!);

    // 5
    entry
        .signals
        .where((element) => element.length == 5)
        .forEach((element) {
      List<String> result = minus(mapping[7]!, element);
      if (result.length == 1) {
        mapping[5] = element;
        digit.order[2] = result[0];
      }
    });
    entry.signals.remove(mapping[5]!);

    // 3
    mapping[3] =
        entry.signals.where((element) => element.length == 5).first;
    entry.signals.remove(mapping[3]!);

    // 6
    entry
        .signals
        .where((element) => element.length == 6)
        .forEach((element) {
      List<String> result = minus(element, mapping[7]!);
      if (result.length == 4) {
        mapping[6] = element;
      } else {
        mapping[0] = element;
      }
    });

    String tempSum = '';
    entry.values.forEach((value) {
      tempSum += mapping.keys.firstWhere((key) =>
      mapping[key]!.length == value.length && minus(mapping[key]!, value).length == 0
      ).toString();
    });
    sum += int.parse(tempSum);
  });
  return sum.toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

class Entry {
  List<String> signals;
  List<String> values;

  Entry(this.signals, this.values);
}

class Digit {
  final List<String> order = List.generate(7, (index) => '.');

  draw() {
    print(' ' + order[0] + order[0] + order[0] + order[0] + ' ');
    print(order[1] + '    ' + order[2]);
    print(order[1] + '    ' + order[2]);
    print(' ' + order[3] + order[3] + order[3] + order[3] + ' ');
    print(order[4] + '    ' + order[5]);
    print(order[4] + '    ' + order[5]);
    print(' ' + order[6] + order[6] + order[6] + order[6] + ' ');
  }
}

List<String> characters(String toSplit) {
  return toSplit.split('');
}

List<String> minus(String one, String two) {
  List<String> temp = characters(one);
  temp.removeWhere((element) => characters(two).contains(element));

  return temp;
}

List<String> minusString(List<String> one, String two) {
  List<String> temp = one;
  temp.removeWhere((element) => characters(two).contains(element));

  return temp;
}
