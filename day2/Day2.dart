import 'dart:io';

String part1(List<String> lines) {
  int horizontal = 0;
  int depth = 0;

  lines.forEach((element) {
    String direction = element.split(' ')[0];
    int amount = int.parse(element.split(' ')[1]);

    switch(direction) {
      case 'forward':
        horizontal += amount;
        break;
      case 'down':
        depth += amount;
        break;
      case 'up':
        depth -= amount;
        break;
    }
  });

  return (horizontal * depth).toString();
}

String part2(List<String> lines) {
  int horizontal = 0;
  int depth = 0;
  int aim = 0;

  lines.forEach((element) {
    String direction = element.split(' ')[0];
    int amount = int.parse(element.split(' ')[1]);

    switch(direction) {
      case 'forward':
        horizontal += amount;
        depth += amount * aim;
        break;
      case 'down':
        aim += amount;
        break;
      case 'up':
        aim -= amount;
        break;
    }
  });

  return (horizontal * depth).toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}
