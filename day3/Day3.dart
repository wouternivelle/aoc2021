import 'dart:io';

import 'dart:math';

String part1(List<String> lines) {
  String gamma = '';
  String epsilon = '';

  int size = lines[0].length;

  for (int i = 0; i < size; i++) {
    int countZero = 0;
    int countOne = 0;
    String bit = '';
    lines.forEach((element) {
      bit = element[i];
      if (bit == '0') {
        countZero++;
      } else {
        countOne++;
      }
    });

    if (countZero < countOne) {
      gamma += '1';
      epsilon += '0';
    } else {
      gamma += '0';
      epsilon += '1';
    }
  }

  return (int.parse(gamma, radix: 2) * int.parse(epsilon, radix: 2)).toString();
}

String part2(List<String> lines) {
  String oxygenPart = '';
  String scrubberPart = '';

  int size = lines[0].length;

  List<String> rest = lines;
  String oxygen = '';
  for (int i = 0; i < size; i++) {
    int countZero = 0;
    int countOne = 0;
    String bit = '';
    rest.forEach((element) {
      bit = element[i];
      if (bit == '0') {
        countZero++;
      } else {
        countOne++;
      }
    });

    if (countZero <= countOne) {
      oxygenPart += '1';
    } else {
      oxygenPart += '0';
    }

    rest = lines.where((element) => element.startsWith(oxygenPart)).toList();
    if (rest.length <= 1) {
      oxygen = rest[0];
      break;
    }
  }

  rest = lines;
  String scrubber = '';
  for (int i = 0; i < size; i++) {
    int countZero = 0;
    int countOne = 0;
    String bit = '';
    rest.forEach((element) {
      bit = element[i];
      if (bit == '0') {
        countZero++;
      } else {
        countOne++;
      }
    });

    if (countZero <= countOne) {
      scrubberPart += '0';
    } else {
      scrubberPart += '1';
    }

    rest = lines.where((element) => element.startsWith(scrubberPart)).toList();
    if (rest.length <= 1) {
      scrubber = rest[0];
      break;
    }
  }

  return (int.parse(oxygen, radix: 2) * int.parse(scrubber, radix: 2)).toString();
}

readInput(filePath) {
  return new File(filePath).readAsLinesSync();
}

void main() {
  print('Part 1: ' + part1(readInput('input.txt')));
  print('Part 2: ' + part2(readInput('input.txt')));
}
